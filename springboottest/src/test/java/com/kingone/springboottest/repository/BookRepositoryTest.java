package com.kingone.springboottest.repository;

import com.kingone.springboottest.entity.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    void findAll(){
        System.out.println(bookRepository.findAll());
    }

    @Test
    void save(){
        Book book = new Book();
        book.setName("SpringBoot");
        book.setAuthor("KingOne");
        book.setPublish("CQU2");
        Book save = bookRepository.save(book);
        System.out.println(save);
    }

    @Test
    void findById(){
        Book book = bookRepository.findById(1).get();
        System.out.println(book);
    }

    @Test
    void update(){
        /*注意此处的save()方法时通过id进行对应的设置，如果未设置，则内容为空。*/
        Book book = new Book();
        book.setId(23);
        book.setName("SPRT");
        bookRepository.save(book);
    }

    @Test
    void delete(){
        bookRepository.deleteById(24);
    }
}