package com.kingone.springboottest.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity     //使用此注解通过类名“book”和数据库中的“book”对应绑定
@Data   //自动生成getter setter
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)     /*注意IDENTITY表示主键由数据库自动生成*/
    private Integer id;
    private String name;
    private String author;
    private String publish;
}
