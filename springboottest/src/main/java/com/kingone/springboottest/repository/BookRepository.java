package com.kingone.springboottest.repository;

import com.kingone.springboottest.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

/*此处直接通过继承JpaRepositor对象，通过自带的方法完成查询*/
/*<Book,Integer>分别表示<要操作的对象名，主键类型>*/
public interface BookRepository extends JpaRepository<Book,Integer> {

}
