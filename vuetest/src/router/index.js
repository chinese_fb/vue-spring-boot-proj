import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import App from '../App.vue'

import Hello from '../views/Hello.vue'
import BookManage from '../views/BookManage.vue'
import AddBook from '../views/AddBook.vue'
import UpdateBook from '../views/UpdateBook.vue'
import PageThree from '../views/PageThree.vue'
import PageFour from '../views/PageFour.vue'
import Index from '../views/Index.vue'

Vue.use(VueRouter)

const routes = [
  /*{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/!* webpackChunkName: "about" *!/ '../views/About.vue')
  },*/
    {
      path:'/',
      name:'图书管理',
      component:Index,
      show:true,      //用于控制是否遍历
      redirect: '/bookManage',     /*此处为重定向；即在访问path时，发生二次跳转到redirect路径*/

      //  meta: { navShow: true, cname: '一级页面' },

        /*正确理解children的作用：用于子页面的展示*/
      children:[
           {
               path:'/bookManage',
               name:'查询图书',
               component:BookManage
           },
           {
               path:'/addBook',
               name:'添加图书',
               component:AddBook
           },
          {
              path:'/updateBook',
              component:UpdateBook,

            //  meta: { navShow: false, cname: '二级页面' },
          },
       ]
    },

    {
        path:'/navigation',
        name:'操作导航2',
        component:Index,
        show:true,
        children:[
            {
                path:'/pageThree',
                name:'其他操作1',
                component:PageThree
            },
            {
                path:'/pageFour',
                name:'其他操作2',
                component:PageFour
            }
        ]
    },
    {
        path:'/hello',
        name:'Hello',
        component:Hello,
        show:true,
    },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
